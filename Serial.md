# Connecting Serial Device using LoopEdge

Any Serial device can be connected to the cloud using simple drag and drop feature of LoopFlows

Reads data from a local serial port.
Can either
wait for a "split" character (default \n). Also accepts hex notation (0x0a).
Wait for a timeout in milliseconds for the first character received
Wait to fill a fixed sized buffer
It then outputs msg.payload as either a UTF8 ascii string or a binary Buffer object.

If no split character is specified, or a timeout or buffer size of 0, then a stream of single characters is sent - again either as ascii chars or size 1 binary buffers.

## STEP 1:

Open LoopFlows Application inside LoopEdge

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Modbus/loopflow.png)

## STEP 2:

Drag and drop Serial from the nodes under Litmus tab.Use msg.payload to debug the readings.

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Modbus/nodes.png)

Click on Add new device and define the Baud Rate, Data Bits, Parity and Stop bits. 

Also define the Input to be split *input on the character*, *after a timeout of* or *into fixed length of* and how to deliver it as *ascii strings* or *binary buffers*.

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Modbus/serial.png)

## STEP 3:

Click Save and the Flow will send the data to the cloud. It can be debug in the debug tab. Once the flows are saved they can be seen under the FLow tab.

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Modbus/flow.png)