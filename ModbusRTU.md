# Connect a ModbusRTU serial device

Modbus RTU srial devices can easily send data to the cloud using the Loop flows applicaiton. Loop Flows application is a drag and drop based UI which is avialable by default in LoopEdge. 

ModbusRTU Read Node.
Connects to a Modbus Serial to read registers/coils values.
msg.payload is used as the payload of the read address.
The plc address used can be configured in the node or, if left blank, can be set by msg.readtype,msg.readid,msg.readaddress and msg.readlength.

Eg.
Read Coil Status msg.readtype="1"
Read Input Status msg.readtype="2"
Read Holding Registers msg.readtype="3"
Read Input Registers msg.readtype="4"
For Device/Slave ID 1 msg.readid = "1"
For Address 30021 msg.readaddress = "30021"
For Read Length 3 msg.readlength = "3"


## STEP 1:

Open LoopFlows Application inside LoopEdge

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Modbus/loopflow.png)

## STEP 2:

Drag and drop ModbusRTU from the nodes under Litmus tab. Aslo use the Inject node to inject timestamp or to define the interval between 2 readings. 
Use msg.payload to debug the readings.

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Modbus/nodes.png)

Click on Add a new device icon to define the port, baud rate and the type which can be RTU, RTU Buffered, ASCII Serial

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Modbus/modbus.png)

Define the Defination to be either

* Coil Status
* Input Status
* Holding Register
* Input Register

Define the Slave ID, Address and Length of the registeres to be read from the defined address

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Modbus/modbusdetails.png)

## STEP 3:

Click Save and the Flow will send the data to the cloud. It can be debug in the debug tab. Once the flows are saved they can be seen under the FLow tab.

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Modbus/flow.png)








