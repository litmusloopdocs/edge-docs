# Connecting Siemens PLC to Cloud using LoopEdge

Siemens supports communication with PROFINET IO devices.PROFINET (pitch acronym for Process Field Net) is an industry technical standard for data communication over Industrial Ethernet, designed for collecting data from, and controlling, equipment in industrial systems, with a particular strength in delivering data under tight time constraints (on the order of 1ms or less).

Siemens PLC Tag Read
Connects to a PLC and Read tags.
msg.payload is used as the payload of the read tags.
The plc tag used can be configured in the node or, if left blank, can be set by msg.plctag and msg.tagtype.
Ex.
msg.plctag="MW4"
For Boolean type msg.tagtype="bool"
For Char type msg.tagtype="char"
For Int type msg.tagtype="int"
For Real type msg.tagtype="real"

## STEP 1:

Open LoopFlows Application inside LoopEdge

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Modbus/loopflow.png)

## STEP 2:

Drag and drop Siemens from the nodes under Litmus tab. Aslo use the Inject node to inject timestamp or to define the interval between 2 readings. 
Use msg.payload to debug the readings.

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Modbus/nodes.png)

Click on Add a new device to add a new PLC by defining the PLC IP, Rack number and the Slot.

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Modbus/siemens.png)

Define the parameters of the Siemens PLC.

* Tag Address- Define the tag address
* Data Type- Siemens supports bool, char, int and real data type. Select the input with the appropriate data type
* Name- Give an appropraite name for the PLC

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Modbus/siemens1.png)

## STEP 3:

Click Save and the Flow will send the data to the cloud. It can be debug in the debug tab. Once the flows are saved they can be seen under the FLow tab.

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Modbus/flow.png)