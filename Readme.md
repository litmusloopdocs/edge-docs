# Getting Started

## Step 1:

Download and Install latest Virtual Box from the [link](https://www.virtualbox.org/wiki/Downloads) for your OS

## Step 2:

Open Virtual box and go to File->Import Appliance

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/VM/import.png)

## Step 3:

Select the path where the **loopedge.ova** is stored

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/VM/ImportOva.png)


## Step 4:

One can configure the Name, CPU and the RAM of the virtual machine before Importing.

**Note** The setttings can also be altered after importing the image

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/VM/ApplianceSettings.png)

The **LoopEdge** virtual image will import to the VM in few seconds
 
![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/VM/importing.png)

## Step 5:

One can see **LoopEdge** in the Virtual Box with the Settings on the left

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/VM/LoopEdgeImported.png)

## Step 6:

Configure the network setting, by assigning Bridged adaptor

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/VM/NetworkSettings.png)

Click on Start to load the **LoopedEdege Image** and it will load in few minutes

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/VM/LoopedgeLoading.png)

## Step 7:

Once LoopEdge loads you will see the IP addresss on which you can login to see the web browser

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/VM/ip.png)

## Step 8:

Login to the ip address which is availble on loading the virtual image, to access the webpage

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Dashboard/webpage.png)

login with the default *username:* admin and *password:*loopedge

## Step 9:

Once logged in using the default username and password you will be directed to the License under Help section.

Enter the Product key which is provided by **Litmus Automation** or you can enter the Trail period of 15 days.

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Dashboard/LoginHelp.png)

## Step 10:

After entering the product key or starting the trial period the user will be directed to change the Account details, before proceeding further

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Dashboard/Trail.png)

## Dashboard

Once the user provides his/her custom account details i.e username and password they will be directed to the dashboard as shown

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Dashboard/Dashboard.png)

The user will see the CPU characteristics, Memory consumption and the Network characteristics of the device.

The user can also have an overview of the number of Applications running on **LoopEdge**

## Device

The Device Tab gives the details of the device/gateway on which loopedge is currently running

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Device/Platform_general.png)

### Platform

Under the Platform you can see various **CPU** components like the brand, logical core and the physical core

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Device/PlatformCpu.png)

Under the **Disk** tab you can see the vendor, model name, size of the Disk

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Device/PlatformDisk.png)

The **Memory** tab under Platform gives the details about the memory which includes the Total memory, Module type, Memory size, Vendor

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Device/PlatformMemory.png)

### Network

The Device tab also consists of the **Network** details

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Device/Network.png)

## Serial

Various Serial protocols can be found under the **Serial** tab 

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Device/Serial.png)

## Flow

The **Flow** tab can be used to launch the *LoopFlowsApplication*

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Flow/Flow.png)

The *LoopFlows* can be used to develop various flows using many of the defined functions. 

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Flow/loopFlows.png)

The drag and drop feature of *LoopFlows* makes it easy to implement any complex function

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Flow/flow_example.png)

## Applications

The Applications consists of various Containers, Stacks and Images

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Applications/Application.png)

Various custom applications an be deployed as containers

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Applications/ApplicationDeployment.png)

### Stacks

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Applications/ApplicationStacks.png)

### Images

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Applications/ApplicationImages.png)

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Applications/PullImage.png)

## Marketplace

The Marketpalce consits of varioys public applications as well as private applications

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Marketplace/PublicMarketplace.png)

Initially the user needs to synchronize the Market place to get access to the public applicatons

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Marketplace/Marketplace.png)

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Marketplace/MarketplaceSynced.png)

## Manage

### Users

User management can be done under this tab

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Manage/ManageUsers.png)

New user can be added using **Add Account** tab under Manage-> Users

**NOTE**- User role can also be defined here as user, developer or Administrator

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Manage/Adduserrole.png)

Added users can be edited or deleted as shown

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Manage/useradded.png)

### Marketplace Category

Market place applications can be managed under this section

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Manage/ManageMarketplaceCategory.png)

New private Market place can be added 

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Manage/AddMarketplace.png)

One added one can edit or delte the Marketplace

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Manage/PrivateMarketplace.png)

### Registry

One can add and mange Registry under this section

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Manage/ManageRegistry.png)

Add registry by entering the Registering Details in the tab shown

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Manage/RegistryDetails.png)

## Audit

The Audit tab shows the various logs of the events along with the time when the event occured

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Webpage/Audit/AuditEvents.png)