# Connecting EthernetIP using LoopEdge

EtherNet/IP is an industrial network protocol that adapts the Common Industrial Protocol to standard Ethernet. EtherNet/IP is one of the leading industrial protocols in the United States and is widely used in a range industries including factory, hybrid and process. We can connect EtherNet IP based device to the cloud with a simple drag and drop feature.

## STEP 1:

Open LoopFlows Application inside LoopEdge

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Modbus/loopflow.png)

## STEP 2:

Drag and drop EIP from the nodes under Litmus tab. Aslo use the Inject node to inject timestamp or to define the interval between 2 readings. 
Use msg.payload to debug the readings.

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Modbus/nodes.png)

Click on Add new device icon to add a new Ethernet IP device. Define the Device IP address, the Port number and the slot number of the device.

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Modbus/eip.png)

Define the Following parameters to read data from an Ehternet IP device

* Tag Name- The Tag name used in the PLC
* Data Type- The data type of the input/output
* Name- Give a name to identify the flow

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Modbus/eip1.png)

## STEP 3:

Click Save and the Flow will send the data to the cloud. It can be debug in the debug tab. Once the flows are saved they can be seen under the FLow tab.

![alt text](https://bytebucket.org/litmusloopdocs/edge-docs/raw/master/Modbus/flow.png)